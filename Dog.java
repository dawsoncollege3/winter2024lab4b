public class Dog{
  private String name;
  private String breed;
  private int age;

  //Getter//

  public String getName() {
    return this.name;
  }

  public String getBreed() {
    return this.breed;
  }
  public int getAge() {
    return this.age;
  }

  //Setter//

  public void setName(String name) {
    this.name = name;
  }

  //Constructor//

  public Dog(String name, String breed, int age) {
    this.name = name;
    this.breed = breed;
    this.age = age;
  }

  //Instances//
  
  public String bark(){
    return "WOOF!";
  }
  public String sayHi(){
    return "Howdy, my name is " + name + " I am a " + age + " year old " + breed; 
  }
}